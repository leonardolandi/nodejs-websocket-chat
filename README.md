<h4>Download and setup:</h4>
Download with:
<pre>
git clone https://gitlab.com/leonardolandi/nodejs-websocket-chat.git
</pre>
and edit the two files <i>config.json</i> and <i>public/config.js</i> according to your domain and port.
<br>
Ensure that a HTTP server (for example <i>apache</i>) is running and place the content of the <i>public</i> folder inside your public http folder (it can be <i>/var/www/html</i>).

<h4>Run:</h4>
Install dependencies first:
<pre>
cd nodejs-websocket-chat
npm install
</pre>
and run the server:
<pre>
npm start
</pre>
You should now be able to join the chat from any browser by connecting to the domain you specified.

<h4>Disclaimer:</h4>
This project is intended to be a simple example on how NodeJS and WebSockets work together. There is no kind of user input validation or protection on both client and server sides.
