// Include configuration file
var CONFIG = require("./config.json");

// HTTP server
var http = require("http");
var SERVER = http.createServer( function(request, response) {});
SERVER.listen((CONFIG["PORT"]), CONFIG["HOSTNAME"], function() { console.log((new Date()) + " Server is listening on port " + (CONFIG["PORT"])) });

// WebSocket server
var WebSocket = require("ws");
var wss = new WebSocket.Server({ server: SERVER });

// Globals
var connections = [], nicknames = [];

// Function for sending message to connected users
function sendToAll(mess){ for (var i=0; i<connections.length; i++){ connections[i].send(JSON.stringify(mess)) } }

wss.on("connection", function(connection) {

	connection.on("message", function(message) {
	
		var msg = JSON.parse(message);

		if (msg.logname !== undefined) {
			if (nicknames.includes(msg.logname)) {
				connection.send(JSON.stringify({error: 0}));
			} else {
				connections.push(connection);
				nicknames.push(msg.logname);
				var now = new Date();
				sendToAll({
					nicknames: nicknames,
					logged: msg.logname,
					h: now.getHours(),
					m: now.getMinutes(),
					s: now.getSeconds()
				});
			}
		}
		
		else if (msg.text !== undefined) {
			var now = new Date();
			sendToAll({
				text: msg.text,
				nick: msg.nick,
				h: now.getHours(),
				m: now.getMinutes(),
				s: now.getSeconds()
			});
		}
		
	});

	connection.on("close", function(message) {
		var ind = connections.indexOf(connection);
		connections.splice(ind,1);
		var who = nicknames[ind];
		nicknames.splice(ind,1);
		var now = new Date();
		sendToAll({
			logout: who,
			online: nicknames,
			h: now.getHours(),
			m: now.getMinutes(),
			s: now.getSeconds()
		});
	});
	
});
